package com.example.myredission;

import com.example.myredission.LockResult;

import java.util.List;

/***@description:分布式锁*@author:yanghua*@date:2020/7/816:55*@version：*/
public interface Lock {
    /***获取锁：单个锁的获取**@authoryanghua*@date2020/7/214:23*@paramlockName锁名称*@paramisFairtrue公平锁，false非公平锁*@returnLockResult*@throws*/
    LockResult lock(String lokName, boolean isFair);

    /***获取锁：单个锁的获取(带超时时间)**@authoryanghua*@date2020/7/217:20*@paramlockName锁名称*@paramtimeOut超时时间单位秒*@paramisFairtrue公平锁，false非公平锁*@returnLockResult*@throws*/
    LockResult lock(String lockName, long timeOut, boolean isFair);

    /***获取锁（尝试获取：获取成功/失败立刻返回）**@authoryanghua*@date2020/7/217:35*@paramlockName锁名称*@paramisFairtrue公平锁，false非公平锁*@returnLockResult*@throws*/
    LockResult tryLock(String lockName, boolean isFair);

    /***获取锁（尝试获取：获取失败立刻返回）**@authoryanghua*@date2020/7/217:35*@paramlockNames锁名称列表*@paramisFairtrue公平锁，false非公平锁*@returnLockResult*@throws*/
    LockResult tryLock(List<String> lockNames, boolean isFair);

    /***获取锁：多个所的获取，全部获取成功才表示锁获取成功**@authoryanghua*@date2020/7/214:25*@paramlockNames锁名称列表*@paramisFairtrue公平锁，false非公平锁*@returnLockResult*@throws*/
    LockResult lock(List<String> lockNames, boolean isFair);

    /***获取锁：多个所的获取，全部获取成功才表示锁获取成功(带超时时间)**@authoryanghua*@date2020/7/217:23*@paramlockNames锁名称列表*@paramtimeOut超时时间单位秒*@paramisFairtrue公平锁，false非公平锁*@returnLockResult*@throws*/
    LockResult lock(List<String> lockNames, long timeOut, boolean isFair);

    LockResult lockInterruptibly(String lockName, long timeOut, boolean isFair) throws InterruptedException;

    LockResult lockInterruptibly(String lockName, boolean isFair) throws InterruptedException;

    LockResult lockInterruptibly(List<String> lockNames, long timeOut, boolean isFair) throws InterruptedException;

    LockResult lockInterruptibly(List<String> lockNames, boolean isFair) throws InterruptedException;

    /***释放锁**@authoryanghua*@date2020/7/214:26*@paramLockResult*@returnvoid*@throws*/
    void unlock(LockResult LockResult);
}