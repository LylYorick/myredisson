package com.example.myredission;


/**
 * @description: 获取锁结果
 * @author: yanghua
 * @date: 2020/7/8 16:57
 * @version：
 */
public class LockResult {

    private String seq;

    private Object lock;

    private Boolean isSuccess;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

}