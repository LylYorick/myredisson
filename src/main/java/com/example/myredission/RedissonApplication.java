package com.example.myredission;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

//@SpringBootApplication
public class RedissonApplication implements ApplicationRunner {
    /**
     * 直接注入RedissonClient就可以直接使用.
     */
    @Resource
    private RedissonClient redissonClient;


    public static void main(String[] args) {
        SpringApplication.run(RedissonApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("spring boot run");

        //创建所
        RLock helloLock = redissonClient.getLock("hello");

        //加锁
        helloLock.lock();
        try {
            System.out.println("locked");
            Thread.sleep(1000 * 200);
        } finally {
            //释放锁
            helloLock.unlock();
        }
        System.out.println("finished");
    }

}
