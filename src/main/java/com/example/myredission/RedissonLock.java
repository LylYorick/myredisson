package com.example.myredission;

import ch.qos.logback.classic.Logger;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.beans.Beans;
import java.util.List;
import java.util.concurrent.TimeUnit;
//@Component("redissonLock")
public class RedissonLock implements Lock {
    private static final Logger logger = null;
    private ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Beans.class);
    private static RedissonClient redissonClient;
    /**
     * 默认获取锁的时间，单位秒（3分钟）
     */
    private static Long defaultTimeOut = 180L;

    private RedissonLock() {
        redissonClient = applicationContext.getBean(RedissonClient.class);
    }

    @Override
    public LockResult lock(String lockName, boolean isFair) {
        return getLock(lockName, 0, isFair, false);
    }

    @Override
    public LockResult lock(String lockName, long timeOut, boolean isFair) {
        return getLock(lockName, timeOut, isFair, false);
    }

    @Override
    public LockResult tryLock(String lockName, boolean isFair) {
        return getLock(lockName, 0, isFair, true);
    }

    @Override
    public LockResult tryLock(List<String> lockNames, boolean isFair) {
        return getLock(lockNames, 0, isFair, true);
    }

    private static LockResult getLock(String lockName, long timeOut, boolean isFair, boolean isTryAcquires) {
        LockResult lockResult = new LockResult();
        if (lockName == null || "".equals(lockName)) {
            logger.error("锁名称不能为空");
            lockResult.setSuccess(false);
            return lockResult;
        }

        RLock rLock = null;
        if (isFair) {
            rLock = redissonClient.getFairLock(lockName);
        } else {
            rLock = redissonClient.getLock(lockName);
        }

        if (timeOut <= 0) {
            timeOut = defaultTimeOut;
        }
        if (isTryAcquires) {
            rLock.tryLock();
        } else {
            rLock.lock(timeOut, TimeUnit.SECONDS);
        }

        lockResult.setSuccess(true);
        lockResult.setLock(rLock);

        return lockResult;
    }

    @Override
    public LockResult lock(List<String> lockNames, boolean isFair) {
        return getLock(lockNames, 0, isFair, false);
    }

    @Override
    public LockResult lock(List<String> lockNames, long timeOut, boolean isFair) {
        return getLock(lockNames, timeOut, isFair, false);
    }

    @Override
    public LockResult lockInterruptibly(String lockName, long timeOut, boolean isFair) throws InterruptedException {
        return null;
    }

    @Override
    public LockResult lockInterruptibly(String lockName, boolean isFair) throws InterruptedException {
        return null;
    }

    @Override
    public LockResult lockInterruptibly(List<String> lockNames, long timeOut, boolean isFair) throws InterruptedException {
        return null;
    }

    @Override
    public LockResult lockInterruptibly(List<String> lockNames, boolean isFair) throws InterruptedException {
        return null;
    }

    private LockResult getLock(List<String> lockNames, long timeOut, boolean isFair, boolean isTryAcquires) {
        LockResult lockResult = new LockResult();
        lockResult.setSuccess(false);
        if (lockNames == null || lockNames.size() == 0) {
            logger.error("锁名称不能为空");
            return lockResult;
        }

        RLock[] rLocks = new RLock[lockNames.size()];
        for (int i = 0; i < lockNames.size(); i++) {
            String lockName = lockNames.get(i);
            RLock rLock = null;
            if (isFair) {
                rLock = redissonClient.getFairLock(lockName);
            } else {
                rLock = redissonClient.getLock(lockName);
            }
            rLocks[i] = rLock;
        }

        RLock multiLock = null;
        multiLock = redissonClient.getMultiLock(rLocks);
        if (timeOut <= 0) {
            timeOut = defaultTimeOut;
        }

        if (isTryAcquires) {
            multiLock.tryLock();
        } else {
            multiLock.lock(timeOut, TimeUnit.SECONDS);
        }

        lockResult.setSuccess(true);
        lockResult.setLock(multiLock);

        return lockResult;
    }

    @Override
    public void unlock(LockResult lockResult) {
        if (lockResult == null) {
            logger.error("unLock fail! [RLock is null]");
            return;
        }
        RLock rLock = (RLock) lockResult.getLock();
        rLock.unlock();
    }
}