package com.example.myredission;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.beans.Beans;
import java.util.List;

/**
 * @description: 分布式锁
 * @author: yanghua
 * @date: 2020/7/8 16:51
 * @version：
 */
public class Dlock implements Lock {
    private ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Beans.class);
    private static Lock lock;

    private static Dlock DLOCK ;

    private Dlock() {
        // 获取系统配置
        // TODO
        String lockType = "redissonLock";
        lock = (Lock) applicationContext.getBean(lockType);
    }

    private static class InnerDLock {
        public final static Dlock LOCK = new Dlock();
    }

    public static Dlock create() {
        if (DLOCK == null) {
            DLOCK = InnerDLock.LOCK;
        }

        // TODO
//        if (false) {
//            // 获取系统配置
//            String lockType = "redissonLock";
//            lock = SpringContextHolder.getBean(lockType);
//        }

        return DLOCK;
    }
    @Override
    public LockResult lock(String lockName, boolean isFair) {
        return lock.lock(lockName, isFair);
    }

    @Override
    public LockResult lock(String lockName, long timeOut, boolean isFair) {
        return lock.lock(lockName, timeOut, isFair);
    }

    @Override
    public LockResult tryLock(String lockName, boolean isFair) {
        return lock.lock(lockName, isFair);
    }

    @Override
    public LockResult tryLock(List<String> lockNames, boolean isFair) {
        return lock.lock(lockNames, isFair);
    }

    @Override
    public LockResult lock(List<String> lockNames, boolean isFair) {
        return lock.lock(lockNames, isFair);
    }

    @Override
    public LockResult lock(List<String> lockNames, long timeOut, boolean isFair) {
        return lock.lock(lockNames, timeOut, isFair);
    }

    @Override
    public LockResult lockInterruptibly(String lockName, long timeOut, boolean isFair) throws InterruptedException {
        return lock.lockInterruptibly(lockName, timeOut, isFair );
    }

    @Override
    public LockResult lockInterruptibly(String lockName, boolean isFair) throws InterruptedException {
        return lock.lockInterruptibly(lockName, isFair );
    }

    @Override
    public LockResult lockInterruptibly(List<String> lockNames, long timeOut, boolean isFair) throws InterruptedException {
        return lock.lockInterruptibly(lockNames, timeOut, isFair );
    }

    @Override
    public LockResult lockInterruptibly(List<String> lockNames, boolean isFair) throws InterruptedException {
        return lock.lockInterruptibly(lockNames, isFair );
    }

    @Override
    public void unlock(LockResult LockResult) {
        lock.unlock(LockResult);
    }
}