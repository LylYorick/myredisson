package com.example.myredission;

import org.junit.jupiter.api.Test;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class MyredissionApplicationTests {
    @Resource
    private RedissonClient redissonClient;

    @Test
    public void lock02() throws Exception {
        System.out.println("spring boot run");

        //创建所
        RLock helloLock = redissonClient.getLock("hello");

        //加锁
        helloLock.lock();
        try {
            System.out.println("locked");
            Thread.sleep(1000 * 30);
        } finally {
            System.out.println("begin unlock");
            //释放锁
            helloLock.unlock();
        }
        System.out.println("finished");
    }


    @Test
    public void lock03() throws Exception {
        System.out.println("spring boot run");

        //创建所
        RLock helloLock = redissonClient.getLock("hello");
        //加锁
        helloLock.lock();
        System.out.println("locked");
        try {
            Thread.sleep(1000 * 30);

        } finally {
            System.out.println("begin unlock");
            //释放锁
            helloLock.unlock();
        }
        System.out.println("finished");
    }


}
